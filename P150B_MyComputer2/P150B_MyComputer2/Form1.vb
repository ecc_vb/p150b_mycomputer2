﻿' Project:P150B_MyComputer2
' Auther:IE2A No.24, 村田直人
' Date: 2015年04月24日



Public Class Form1

    '配列の要素数を宣言
    Const Last As Integer = 9

    'ラベル配列の要素数をLastを使って宣言
    Dim ComLabels(Last) As Label

    'ラベルのコピーした回数をカウントする変数
    Dim Counter As Integer

    'ラベルのY座標の初期値を格納する変数
    Dim LabeIntTop As Integer

    'ラベルの配列間隔を格納する変数
    Dim CopyDistance As Integer

    'フォームのサイズ（初期値）を格納しておく変数
    Dim FormInitSize As Size




    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load

        'ラベルY座標を変数に格納
        LabeIntTop = ComLabel.Location.Y

        'ラベルの配置間隔変数にラベルサイズ＋10を格納
        CopyDistance = ComLabel.Size.Width + 10

        'ラベルの参照をComLabelsの先頭の要素に代入
        ComLabels(Counter) = ComLabel

        'フォームのサイズの初期値を格納
        FormInitSize = Me.Size

        'リセットボタンを無効にする
        ResetButton.Enabled = False


    End Sub


    Private Sub ComLabel_Click(ByVal sender As Object, e As EventArgs) Handles ComLabel.Click

        'クリックした回数をカウント
        Static clickNo As Integer


        'ラベルを下げる
        With CType(sender, Label)
            .Location = New Point(.Location.X, .Location.Y + 10)
        End With


        Debug.WriteLine(clickNo.ToString("000") & _
                        ": Index=" & Array.IndexOf(ComLabels, sender).ToString)

    End Sub

    'コピーボタン処理
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles CopyButton.Click

        'リセットボタンを有効にする
        ResetButton.Enabled = True

        'フォームのサイズ拡幅する
        Me.Size = New Size(Me.Size.Width + CopyDistance, Me.Size.Height)

        'カウンターのインクリメント
        Counter = Counter + 1

        '配列の先頭の要素に追加
        ComLabels(Counter) = New Label

        'コピーしたラベルのプロパティを設定
        With ComLabels(Counter)

            .Location = New Point(ComLabels(Counter - 1).Location.X + CopyDistance, LabeIntTop)
            .Size = ComLabel.Size
            .Image = ComLabel.Image
        End With

        'フォームにラベルを追加
        Me.Controls.Add(ComLabels(Counter))

        '新しいラベルのクリックイベントプロシージャに関連付けする
        AddHandler ComLabels(Counter).Click, AddressOf ComLabel_Click

        If Counter = Last Then

            'コピーボタン無効
            CopyButton.Enabled = False

        End If

    End Sub

    'リセットボタンのクリック処理
    Private Sub ResetButton_Click(sender As Object, e As EventArgs) Handles ResetButton.Click

        'フォームのサイズを初期値に戻す
        Me.Size = FormInitSize

        'コピーボタンを有効に
        CopyButton.Enabled = True

        'ラベルをもとの位置に戻す
        ComLabel.Location = New Point(ComLabel.Location.X, LabeIntTop)

        'コピーしたラベルの削除
        For i As Integer = 1 To Counter

            ComLabels(i).Dispose()
            Me.Controls.Remove(ComLabels(i))
            RemoveHandler ComLabels(i).Click, AddressOf ComLabel_Click

        Next

        'カウンターの初期化
        Counter = 0



    End Sub
End Class
